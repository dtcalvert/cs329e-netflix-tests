#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval, create_cache
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract


# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----
    
    # check thorugh the list to make sure that 
    # all movies have have an rmse less than 1
    def test_precision_rmse_less_than_1(self):

        r = open("nsw425-RunNetflix.in", "r")

        w = StringIO()

        rmse = netflix_eval(r, w)

        self.assertLessEqual(rmse, 1)

    def test_first_line_is_movie_type_int(self):

        r = open("nsw425-RunNetflix.in", "r")

        for line in r:

            line = line.strip()

            if line[-1] == ':':
                
                current_movie_id = line.rstrip(':')

                current_movie_id = int(current_movie_id)

                self.assertEqual(type(current_movie_id), int)

            else:

                current_customer_id = line

                current_customer_id = int(current_customer_id)
                self.assertEqual(type(current_customer_id), int)

        r.close()

    def test_lenght_of_input(self):

        r = open("nsw425-RunNetflix.in", "r")

        counter = 0

        for line in r:

            counter += 1

        r.close()

        self.assertGreaterEqual(counter, 1)

        

    # go thorugh all of the caches and make
    # sure that they are not empty 

    def test_check_actual_customer_rating_cache(self):

        self.assertGreater(len(create_cache("cache-actualCustomerRating.pickle")), 0)

    def test_check_average_movie_rating_by_year_cache(self):

        self.assertGreater(len(create_cache("cache-movieAverageByYear.pickle")), 0)

    def test_check_year_customer_rated_movie_cache(self):

        self.assertGreater(len(create_cache("cache-yearCustomerRatedMovie.pickle")), 0)

    def test_check_customer_average_rating_by_year_cache(self):

        self.assertGreater(len(create_cache("cache-customerAverageRatingByYear.pickle")), 0)

    

# ----
# main
# ----			
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
