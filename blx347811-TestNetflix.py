#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----

    def test_eval_1(self):
        r = StringIO("10010:\n1462925\n52050\n650466\n1813166\n2224061\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10010:\n0.49\n0.30\n2.34\n3.18\n1.74\n1.94\n")

    def test_eval_2(self):
        r = StringIO("3152:\n343559\n833912\n2364029\n504059\n2356076\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "3152:\n0.63\n1.64\n0.44\n0.67\n1.39\n1.06\n")

    def test_eval_3(self):
        r = StringIO("17632:\n1557772\n1092189\n809597\n370602\n6669\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "17632:\n0.15\n1.43\n1.02\n0.00\n1.00\n0.90\n")

    def test_eval_4(self):
        r = StringIO("6071:\n680554\n1717067\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "6071:\n0.14\n0.33\n0.25\n")

# ----
# main
# ----			
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
